import argparse

from cli import process_input

def main():
    """Handle command line arguments"""
    # parse command line args
    arg_parser = argparse.ArgumentParser()

    arg_parser.add_argument("-d0", "--start_deimos", help="Deimos visibility start time")
    arg_parser.add_argument("-d1", "--end_deimos", help="Deimos visibility end time")

    arg_parser.add_argument("-p0", "--start_phobos", help="Phobos visibility start time")
    arg_parser.add_argument("-p1", "--end_phobos", help="Phobos visibility end time")

    print(process_input(arg_parser.parse_args()))

main()
