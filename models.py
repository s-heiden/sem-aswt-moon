from const import FORMATTING_SEPARATOR, MINUTES_PER_HOUR, HOURS_PER_DAY

from operator import xor

class Args:
    def __init__(self, d0: str, d1: str, p0: str, p1: str):
        self.start_deimos = d0
        self.end_deimos = d1
        self.start_phobos = p0
        self.end_phobos = p1

class Time:
    def __init__(self, time: str):
        self.hour, self.minute = [int(i) for i in time.split(FORMATTING_SEPARATOR)]

    def __lt__(self, other):
        return self.as_number() < other.as_number()

    def __eq__(self, other):
            return self.as_number() == other.as_number()

    def as_number(self) -> int:
        return MINUTES_PER_HOUR * self.hour + self.minute


class DateTime:
    def __init__(self, day: int, time: Time):
        self.day = day
        self.time = time

    def __lt__(self, other):
        return self.as_number() < other.as_number()

    def __le__(self, other):
        return self.as_number() <= other.as_number()

    def __gt__(self, other):
        return self.as_number() > other.as_number()

    def __eq__(self, other):
        return self.as_number() == other.as_number()

    def as_number(self) -> int:
        return self.day * HOURS_PER_DAY * MINUTES_PER_HOUR + self.time.as_number()

    def yesterday(self):
        return DateTime(self.day - 1, self.time)


class DateTimeInterval:
    def __init__(self, t0: DateTime, t1: DateTime):
        self.t0 = t0
        self.t1 = t1

    def __str__(self):
        return "[{}:{}:{} ~{}, {}:{}:{} ~{}]"\
            .format(self.t0.day, self.t0.time.hour, self.t0.time.minute, self.t0.as_number(),
                    self.t1.day, self.t1.time.hour, self.t1.time.minute, self.t1.as_number())

    def duration(self) -> int:
        return self.t1.as_number() - self.t0.as_number()


class TimeInterval:
    def __init__(self, t0: Time, t1: Time):
        self.t0 = t0
        self.t1 = t1

    def as_date_time_interval(self) -> DateTimeInterval:
        if self.t0 and self.t1:
            if self.t1 < self.t0:
                return DateTimeInterval(DateTime(0, self.t0), DateTime(1, self.t1))
            else:
                return DateTimeInterval(DateTime(0, self.t0), DateTime(0, self.t1))


class CalculationService:
    @staticmethod
    def intersection(a: DateTimeInterval, b: DateTimeInterval) -> int:
        if a.t0 == a.t1 or b.t0 == b.t1:                                                            # zero duration
            return 0
        elif b.t0 <= a.t0 <= b.t1 and b.t0 <= a.t1.yesterday() <= b.t1:                             # double overlap 1
            return b.duration() - (MINUTES_PER_HOUR * HOURS_PER_DAY - a.duration())
        elif a.t0 <= b.t1.yesterday() <= a.t1 and b.t0 <= a.t1 <= b.t1:                             # double overlap 2
            return a.duration() - (MINUTES_PER_HOUR * HOURS_PER_DAY - b.duration())
        elif a.t1 < b.t0 or a.t0 > b.t1:                                                            # no overlap
            return 0
        elif xor(a.t1.time == b.t0.time, a.t0.time == b.t1.time):                                   # twilight rule
            return 1
        elif a.t0 < b.t0 and a.t1 > b.t1:                                                           # a contains b
            return b.duration()
        elif a.t0 > b.t0 and a.t1 < b.t1:                                                           # a contained in b
            return a.duration()
        elif a.t0 < b.t0 and a.t1 < b.t1:                                                           # right overlap
            return DateTimeInterval(b.t0, a.t1).duration()
        elif a.t0 > b.t0 and a.t1 > b.t1:                                                           # left overlap
            return DateTimeInterval(a.t0, b.t1).duration()

