from models import TimeInterval, CalculationService, Time


def process_input(args) -> int:
    if args.start_deimos and args.end_deimos and args.start_phobos and args.end_phobos:
        d_itv = TimeInterval(Time(args.start_deimos), Time(args.end_deimos)).as_date_time_interval()

    p_itv = TimeInterval(Time(args.start_phobos), Time(args.end_phobos)).as_date_time_interval()

    cs = CalculationService()
    return cs.intersection(d_itv, p_itv)

