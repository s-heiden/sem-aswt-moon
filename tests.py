import unittest

from cli import process_input
from models import Args


class CalculationServiceTestCase(unittest.TestCase):

    def test_should_return_zero_if_D_ends_before_P_starts(self):
        result = process_input(Args("07:52", "09:11", "12:05", "24:51"))
        self.assertEqual(result, 0)

    def test_should_return_zero_if_P_ends_before_D_starts(self):
        result = process_input(Args("12:07", "24:23", "07:97", "09:11"))
        self.assertEqual(result, 0)

    def test_should_return_right_overlap(self):
        result = process_input(Args("08:41", "13:07", "12:01", "22:80"))
        self.assertEqual(result, 106)

    def test_should_return_left_overlap(self):
        result = process_input(Args("08:75", "09:25", "01:00", "08:97"))
        self.assertEqual(result, 22)

    def test_should_return_duration_of_P_if_D_contains_P(self):
        result = process_input(Args("08:77", "18:03", "12:15", "14:15"))
        self.assertEqual(result, 200)

    def test_should_return_duration_of_D_if_P_contains_D(self):
        result = process_input(Args("08:77", "18:72", "07:94", "19:61"))
        self.assertEqual(result, 995)

    def test_should_return_dur_of_two_overlaps_if_yesterdays_D_END_is_after_P_START_and_D_START_is_before_P_END(self):
        result = process_input(Args("12:00", "18:00", "17:00", "13:00"))
        self.assertEqual(result, 200)

    def test_should_return_dur_of_two_overlaps_if_yesterdays_P_END_is_after_D_START_and_P_START_is_before_D_END(self):
        result = process_input(Args("17:00", "13:00", "12:00", "18:00"))
        self.assertEqual(result, 200)

    def test_should_return_1_if_twilight_on_the_left(self):
        result = process_input(Args("17:07", "13:87", "15:65", "17:07"))
        self.assertEqual(result, 1)

    def test_should_return_0_if_twilight_on_both_sides_and_D_reaches_over_daybreak(self):
        result = process_input(Args("17:07", "13:87", "13:87", "17:07"))
        self.assertEqual(result, 0)

    def test_should_return_0_if_twilight_on_both_sides_and_P_reaches_over_daybreak(self):
        result = process_input(Args("01:01", "00:97", "00:97", "01:01"))
        self.assertEqual(result, 0)

    def test_should_return_overlap_on_left_side_if_overlap_on_the_left_and_twilight_on_the_right(self):
        result = process_input(Args("12:32", "17:06", "17:06", "15:78"))
        self.assertEqual(result, 346)

    def test_should_return_overlap_on_right_side_if_overlap_on_the_right_and_twilight_on_the_left(self):
        result = process_input(Args("17:06", "15:78", "12:32", "17:06"))
        self.assertEqual(result, 346)

    def test_should_return_zero_if_duration_of_D_is_zero(self):
        result = process_input(Args("17:06", "17:06", "12:32", "17:06"))
        self.assertEqual(result, 0)

    def test_should_return_zero_if_duration_of_P_is_zero(self):
        result = process_input(Args("17:06", "15:78", "15:32", "15:32"))
        self.assertEqual(result, 0)
