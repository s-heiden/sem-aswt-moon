# How to run
 ```
 usage: moon.py [-h] [-d0 START_DEIMOS] [-d1 END_DEIMOS] [-p0 START_PHOBOS]
                [-p1 END_PHOBOS]
 ```
 
# How to test
```
nosetests    
```
 
# Contained tests
#### TEST 1: should return zero if D ends before P starts
Input = {07:52, 09:11, 12:05, 24:51}

Expected result: 0

 #### TEST 2: should return zero if P ends before D starts
Input = {12:07, 24:23, 07:97, 09:11}

Expected result: 0

 #### TEST 3: should return right overlap
Input = {08:41, 13:07, 12:01, 22:80}

Expected result: 106

 #### TEST 4: should return left overlap
Input = {08:75, 09:25, 01:00, 08:97}

Expected result: 22

 #### TEST 5: should return duration of P if D contains P
Input = {08:77, 18:03, 12:15, 14:15}

Expected result: 200

 #### TEST 6: should return duration of D if P contains D
Input = {08:77, 18:72, 07:94, 19:61}

Expected result: 995

 #### TEST 7: should return dur of two overlaps if yesterdays D END is after P START and D START is before P END
Input = {12:00, 18:00, 17:00, 13:00}

Expected result: 200

 #### TEST 8: should return dur of two overlaps if yesterdays P END is after D START and P START is before D END
Input = {17:00, 13:00, 12:00, 18:00}

Expected result: 200

 #### TEST 9: should return 1 if twilight on the left
Input = {17:07, 13:87, 15:65, 17:07}

Expected result: 1

 #### TEST 10: should return 0 if twilight on both sides and D reaches over daybreak
Input = {17:07, 13:87, 13:87, 17:07}

Expected result: 0

 #### TEST 11: should return 0 if twilight on both sides and P reaches over daybreak
Input = {01:01, 00:97, 00:97, 01:01}

Expected result: 0

 #### TEST 12: should return overlap on left side if overlap on the left and twilight on the right
Input = {12:32, 17:06, 17:06, 15:78}

Expected result: 346

 #### TEST 13: should return overlap on right side if overlap on the right and twilight on the left
Input = {17:06, 15:78, 12:32, 17:06}

Expected result: 346

 #### TEST 14: should return zero if duration of D is zero
Input = {17:06, 17:06, 12:32, 17:06}

Expected result: 0

 #### TEST 15: should return zero if duration of P is zero
Input = {17:06, 15:78, 15:32, 15:32}

Expected result: 0